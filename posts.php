<?php
/**
 * Created by PhpStorm.
 * User: Roli
 * Date: 2016. 11. 16.
 * Time: 0:09
 */

$host = "localhost";
$user = "root";
$pw = "";
$dbname = "olimpia";

$sql = 'select * from posts order by time desc;';

$conn = mysqli_connect($host, $user, $pw, $dbname);

if(!$conn){
    die('DB connection failed');
} else {
    mysqli_stmt_init($conn);
    $stmt =  mysqli_prepare($conn, $sql);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);

    echo "<div id='posts'>";
    if($result === null){
        echo "nincsenek postok";
    } else {
        while($row = mysqli_fetch_array($result)){
            echo "<div id='" . $result['id'] . "' class='post'>";
            echo "<span>" . date('Y-m-d', $result['time']) . "</span><br>";
            echo "<span>" . $result['cont'] . "</span>";
            echo "</div>";
        }
        echo "</div>";
    }
    mysqli_stmt_close($stmt);
}
?>