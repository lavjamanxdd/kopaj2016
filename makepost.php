<?php
/**
 * Created by PhpStorm.
 * User: Roli
 * Date: 2016. 11. 16.
 * Time: 0:42
 */

$host = "localhost";
$user = "root";
$pw = "";
$dbname = "olimpia";

$sql = 'insert into posts values (?, ?)';

$conn = mysqli_connect($host, $user, $pw, $dbname);

if(!$conn){
    die('DB connection failed');
} else {
    $time = time();
    $cont = $_POST['post'];

    $stmt = mysqli_stmt_init($conn);
    $stmt = mysqli_prepare($conn, $sql);
    mysqli_stmt_bind_param($stmt, 'is', $time, $cont);
    mysqli_stmt_execute($stmt);

    header('Location: index.html');
    mysqli_stmt_close($stmt);
    exit();
}
