<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Kopaj 2016 olimpia</title>
        <link rel="stylesheet" href="main.css">
    </head>
    <body>
        <div>
            <table>
            <td>
                <img width="200" height="200" src="assets/kopaj_logo.png" /></td>
            <td style="padding-left:50px"><h1>Olimpia micro-blog</h1> </td>
            </table>
        </div>
        
        <div> 
            <div class="stack-horz">
            <div class="content" id="posts.php">Posts</div>
            <div class="content" id="about-us.html" >About us!</div>
            <div class="content" id="faq.html" >F.A.Q.</div>
            <div class="content" id="contact.html" >Contacts</div>
<?php if(!isset($_SESSION['user'])) : ?>
            <div class="content" id="loginform.html" >Bejelentkezés</div>
            <div class="content" id="regform.html" >Regisztráció</div>
<?php endif; ?>
            </div>
        </div>
        <div id="siteloader"></div>
        <script src="http://code.jquery.com/jquery-1.11.0.min.js" type="text/javascript"></script>
        <script src="scripts.js" type="text/javascript"></script>
            </body>
</html>